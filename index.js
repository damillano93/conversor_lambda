exports.handler = async (event) => {
    // declara las variables
    let valor = event.valor;
    let unidad = event.unidad;
    let convertira = event.convertira;
    let conversion = 0;
    if(unidad == "newton"){
        
        if(convertira=="dinas"){
            conversion = newton_dinas(valor);
            
        }
        if(convertira=="libras"){
           conversion = newton_libras(valor);
            
        }
        if(convertira=="newton"){
            conversion = valor;
             
         }
    }
    if(unidad == "dinas"){
        
        if(convertira=="newton"){
            conversion = dinas_newton(valor);
            
        }
        if(convertira=="libras"){
            conversion = dinas_libras(valor);
            
        }
        if(convertira=="dinas"){
            conversion = valor;
             
         }

    }
    if(unidad == "libras"){
        
        if(convertira=="dinas"){
            conversion = libras_dinas(valor);
            
        }
        if(convertira=="newton"){
            conversion = libras_newton(valor);
            
        }
        if(convertira=="libras"){
            conversion = valor;
             
         }
    }
    const response = {
        statusCode: 200,
        original_value:JSON.stringify(valor),
        original_unit:unidad,
        convert_value: JSON.stringify(conversion),
        convert_unit:convertira
        
    };
    return response;
 
};
   function newton_dinas(valor){
      var convertido = valor*100000
      return convertido
    }
    function newton_libras(valor){
     var convertido = valor*0.224809;
     return convertido
    }
     function dinas_newton(valor){
     var convertido = valor/100000;
     return convertido
    }
    function dinas_libras(valor){
        var convertido = valor*0.0000022480894309971
        return convertido
    }
     function libras_newton(valor){
     var convertido = valor*4.4482216152605;
     return convertido
    }
    function libras_dinas(valor){
     var convertido = valor*444822.16;
     return convertido
    }